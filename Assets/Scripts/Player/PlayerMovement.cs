using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController2D controller;
    public float runSpeed = 40f;
    public Animator Animator;
    public ParticleSystem DamageEffect;
    float horizontalMove;
    bool jump = false;

    // Update is called once per frame
    void Update() {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        if (Input.GetButtonDown("Jump")){
            jump = true;
            Animator.SetBool("Jump", true);
        }
        
        // send speed to animator for walk animation
        Animator.SetFloat("Speed", Mathf.Abs(horizontalMove));
    }

    void FixedUpdate() {
        // move our character
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;
    }

    public void OnLanding() {
        Animator.SetBool("Jump", false);
    }

    void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.layer == LayerMask.NameToLayer("Harm")) {
           DamageEffect.Play();
        };
    }
}
